////////////////////////////////////////////////////////////////////////////////
Class @LinReg.Normal
////////////////////////////////////////////////////////////////////////////////
{
  Real varNum = 10;
  Real dataNum = 500; 
  Real rank = 9;
  Real missingNum = 3;

  NameBlock Create(Real void)
  {
    Set aux = [[
      Real n = varNum;
      Real m = dataNum;
      Real r = rank;
      Set LinBlk.names = For(1,n,Text(Real k)
      {
        "LinBlk_"+FormatReal(k,"%09.0lf")
      });
      
      Real sigma = Rand(0.01,0.10);
      Real sigma2 = sigma^2;
      VMatrix E = Gaussian(dataNum,1,0,sigma);
      VMatrix W = E;
      VMatrix Z = E;
      Serie e = MatSerSet(VMat2Mat(E,true),Daily,y2013)[1]; 
      Serie w = e; 
      Serie z = e; 

      VMatrix b = Rand(n,1,-.5,.5);
    
      VMatrix X = {
        Real m = VRows(Z);
        Set Q = SetOfRation(1/1,(1-B)/1,1/(1-.95*B),1/(1-0.9999*B));
        VMatrix Xr = Group("ConcatColumns",For(1,r,VMatrix(Real k)
        {
          VMatrix x = Constant(0,0,?);
          Real range = 0;
          While(range==0,
          {
            Real p = Rand(0,1); 
            Ratio q = Q[Case(p<=0.7,1,p<=0.9,2,p<=.97,3,p<=1,4)];
            VMatrix x := 
              Round(DifEq(q,Rand(m,1,-5,5,"Cholmod.R.Sparse",IntRand(1,3))))*10/r;
            Real range := VMatMax(x)-VMatMin(x)
          });
          x*(StDsS(w)/range)*Rand(5,8)*2
        }))+
        Triplet(Rand(r,1,1,m)|SetCol(Range(1,r,1))|Rand(r,1,5,8),m,r);
        VMatrix Xn = If(r==n,Xr,
        {
          VMatrix comb = Rand(r,n-r,-.1,.1,"Cholmod.R.Sparse",n-r);
          VMatrix X2 = Xr * comb + Gaussian(m,n-r,0,sigma/100);
          Xr | X2
        }) /*;
        VMatrix F = Xn*b;
        Xn*(sigma/VMatStDs(F))*Rand(3,5) */
    
      };
    
    
      VMatrix Y.full = X*b+Z;
      Matrix missingRows = {
        Matrix prm = RandPermutation(1,VRows(Z)-2)+1;
        Matrix ms = Sub(Tra(prm),1,1,missingNum,1);
        PivotByRows(ms,Sort(ms,[[1]]))
      };
      VMatrix missing = Triplet(
        missingRows|(missingRows*0+1)|(missingRows*0+1),
        VRows(Z),1);
      VMatrix Y.missed = SubRow(Y.full,MatSet(Tra(missingRows))[1]);
      VMatrix Y = IfVMat(missing,?,Y.full);
      Serie y = MatSerSet(VMat2Mat(Y,True),Dating(z),First(z))[1];
      Serie F = y-z;
      Set inputs = For(1,VColumns(X),Set(Real j)
      {
        Serie Xj = MatSerSet(VMat2Mat(SubCol(X,[[j]]),True),Dating(z),First(z))[1];
        InputDef(1E-6,Eval("Inp"<<j+"=Xj"))
      });
    
    //Set varNames = [["sigma2"]]
      VMatrix param = 
        Constant(1,1,sigma2) <<
        b << 
        Y.missed

    /* */
    ]];
    SetToNameBlock(aux)
  }
};
 
