////////////////////////////////////////////////////////////////////////////////
  Class @VAR.Normal
////////////////////////////////////////////////////////////////////////////////
{
  Real numOutput = 3;
  Real maxDegree = 3;
  Real dataNum = 500; 
  Real hasConstant = 1;
  Real linNum = 0;
  Real minHeterochedasticRatio = 1.5;
  Real period = 1;
  Real missingNum = 0;
  Set PHI_0 = Copy(Empty);
  Set PHI = Copy(Empty);
  Set cte = Copy(Empty);
  Set PHIm = Copy(Empty);
  Set roots = Copy(Empty);
  Polyn determinant = 1;
  Real isStationary = False;
  Real accepted = False;

  Set LinBlk.names = Copy(Empty);
  VMatrix covariance = Constant(0,0,?);
  VMatrix correlation = Constant(0,0,?);
  Set beta = Copy(Empty);
  Set X    = Copy(Empty);
  VMatrix A  = Constant(0,0,?);
  VMatrix B  = Constant(0,0,?);
  VMatrix e  = Constant(0,0,?);
  VMatrix e_ = Constant(0,0,?);
  VMatrix F  = Constant(0,0,?);
  VMatrix C  = Constant(0,0,?);
  VMatrix Y0 = Constant(0,0,?);
  VMatrix Y  = Constant(0,0,?);
  VMatrix Y_fcst = Constant(0,0,?);
  Set BkY = Copy(Empty);
  Set param = Copy(Empty);
  Date firstIniVal = Today;
  Date firstAvail = Today;
  Date lastAvail = Today;
  Set crossLinearInput = Copy(Empty);
  Set output = Copy(Empty);
  Set residuals = Copy(Empty);
  Set garchDef = Copy(Empty);
  Set garchInfo = Copy(Empty);
  VMatrix heterokedasticity = Constant(0,0,?);
  VMatrix homokedasticResiduals = Constant(0,0,?);


  //////////////////////////////////////////////////////////////////////////////
  Real Build(Real void)
  //////////////////////////////////////////////////////////////////////////////
  {
  //WriteLn("TRACE [@VAR.Normal::Build] 1");
    Set PHI_0 := For(1,numOutput,Set(Real i)
    {
      For(1,numOutput, Polyn(Real j)
      {  
        (i==j)-SetSum(For(1,maxDegree,Polyn(Real k)
        {
          0.01*B^(k*period)
        }))
      })  
    });   
    Real isStationary := False;
    Real try =0;
    Real m = If(VRows(F),VRows(F),dataNum*5+1000);
    Real isStationary := False;
    Real accepted:= False;
  //WriteLn("TRACE [@VAR.Normal::Build] 2");
    While(!accepted,
    {
      Real try := try + 1;
      Matrix x = VariableChange::@StableMatrixPolyn::
        SquareSetGen.Rand(numOutput, maxDegree, -1, 1, 0.005);
      Set PHI := VariableChange::@StableMatrixPolyn::
        DuffinSchurBinFact(numOutput, maxDegree, x);
      
      Polyn determinant := MPM::@ModuleVAR::PolynMatrixDeterminant(PHI);
      Set roots := MPM::@ModuleVAR::PolynRootsReport(determinant);
      Real isStationary := IsStationary(determinant);
      Real accepted := (roots[1])::module>1.05;
      WriteLn("[@VAR.Normal] (Try "<<try+") "
        +"minimum root module:"<<(roots[1])::module+" ... "+
        If(!isStationary,"NOT SATIONARY","STATIONARY") );
      0
    });
  //WriteLn("TRACE [@VAR.Normal::Build] 3");

    Set PHIm := For(1,maxDegree,Matrix(Real k)
    {
      SetMat(For(1,numOutput,Set(Real i)
      {
        For(1,numOutput, Real(Real j)
        {  
          -Coef(PHI[i][j],k)
        })  
      }))
    });
    Set garchInfo := Copy(Empty);
  //WriteLn("TRACE [@VAR.Normal::Build] 3");
    VMatrix e := If(!Card(garchDef),
    {   
      VMatrix A := Gaussian(m, numOutput, 0, 30/m);
      VMatrix B := Rand(numOutput,numOutput,-.50,.50,"Cholmod.R.Sparse",0.25*numOutput^2);
      VMatrix covariance := MtMSqr(A*(Eye(numOutput)+B));
      VMatrix correlation := Mat2VMat(NormDiag(VMat2Mat(covariance)));
      Matrix cov.L = Choleski(VMat2Mat(covariance));
      Matrix e01 = Gaussian(numOutput, m, 0, 1);
      Mat2VMat(cov.L*e01,True)
    },
    {
      VMatrix A := Gaussian(m, numOutput, 0, 30/m);
      VMatrix B := Rand(numOutput,numOutput,-.50,.50,"Cholmod.R.Sparse",0.25*numOutput^2);
      VMatrix covariance := MtMSqr(A*(Eye(numOutput)+B));
      VMatrix correlation := Mat2VMat(NormDiag(VMat2Mat(covariance)));
      Matrix cov.L = Choleski(VMat2Mat(correlation));
   
      Set garchInfo := For(1,numOutput,NameBlock(Real k)
      {
        generateGarch(garchDef[1], m, dataNum, minHeterochedasticRatio)
      });
      Matrix s = SetRow(For(1,numOutput,Real(Real k)
      {
        (garchInfo[k])::stdDevAvr
      }));
      VMatrix H = Eye(numOutput,numOutput,0,Mat2VMat(s));
      VMatrix covariance := H * correlation * H;
      VMatrix heterokedasticity := Group("ConcatColumns",
      For(1,numOutput,VMatrix(Real k)
      {
        Mat2VMat((garchInfo[k])::stdDev) 
      }));    
      Matrix v = Group("ConcatColumns",
      For(1,numOutput,Matrix(Real k)
      {
        (garchInfo[k])::residual
      }));
     
      Mat2VMat(cov.L * Tra(v), True)
    });
  //WriteLn("TRACE [@VAR.Normal::Build] 4");
    VMatrix C := If(!hasConstant, Constant(m,numOutput,0),
    {
      Set cte := If(!hasConstant,Copy(Empty),For(1,numOutput,Real(Real k)
      {
        Rand(3.5, 4.5)
      }));
      Group("ConcatColumns",For(1,numOutput,VMatrix(Real k)
      {
        Constant(m,1,cte[k])
      }))
    });
  //WriteLn("TRACE [@VAR.Normal::Build] 5");
    VMatrix If(!VRows(F), F := 
    If(!linNum, Constant(m,numOutput,0),
    {
      Set beta := For(1,numOutput,VMatrix(Real i)
      {
        Rand(linNum,1,-2,2)
      });
      Set LinBlk.names := For(1,linNum,Text(Real k)
      {
        "LinBlk_"+FormatReal(k,"%09.0lf")
      });    
      Set X := For(1,numOutput,VMatrix(Real i)
      {
        Round(Rand(m,linNum,-10,10,"Cholmod.R.Sparse",0.30*m*linNum))
      });
      Group("ConcatColumns",For(1,numOutput,VMatrix(Real i)
      {
        X[i]*beta[i]
      }))
    }));
  //WriteLn("TRACE [@VAR.Normal::Build] 6");
    Set y = For(1,m,Matrix(Real t)
    {
      Rand(numOutput,1,-5,5)
    });
    Matrix CFe = Tra(VMat2Mat(C+F+e));
  //WriteLn("TRACE [@VAR.Normal::Build] 7");
    Set For(1+maxDegree,m,Real(Real t)
    {
      Matrix y[t] := SetSum(For(1,maxDegree,Matrix(Real d)
      {
        PHIm[d]*y[t-d] 
      })) + SubCol(CFe,[[t]]);
      True
    });
  //WriteLn("TRACE [@VAR.Normal::Build] 8");
    VMatrix C := Sub(C, m-dataNum+1,1,dataNum,numOutput);
    VMatrix F := Sub(F, m-dataNum+1,1,dataNum,numOutput);
    VMatrix e := Sub(e, m-dataNum+1,1,dataNum,numOutput);
    VMatrix Y0 := Mat2VMat(Group("ConcatRows",For(1,maxDegree,Matrix(Real t)
    {
      Tra(y[t+m-maxDegree-dataNum])
    })));
    VMatrix Y := Mat2VMat(Group("ConcatRows",For(1,dataNum,Matrix(Real t)
    {
      Tra(y[t+m-dataNum])
    })));
    Date firstIniVal := Succ(firstAvail, Daily, -maxDegree);
    Date lastAvail := Succ(firstAvail, Daily, dataNum-1);
    Set residuals := MatSerSet(VMat2Mat(e,True),Daily,firstAvail);
    Set output := MatSerSet(VMat2Mat(Y0<<Y,True),Daily,firstIniVal);
  //WriteLn("TRACE [@VAR.Normal::Build] 9");
    Set BkY := For(1,maxDegree,VMatrix(Real k)
    {
      VMatrix z = Mat2VMat(Group("ConcatRows",For(1,dataNum,Matrix(Real t)
      {
        Tra(y[t+m-k-dataNum])
      })));
      Eval("B"<<k+"Y=z")
    });
    VMatrix e_ := Y - SetSum(For(1,maxDegree,VMatrix(Real k)
    {
      BkY[k]*Mat2VMat(PHIm[k],True)
    })) - F - C;
    VMatrix Y_fcst := Y-e;
  //WriteLn("TRACE [@VAR.Normal::Build] 10");
    Set param := 
    SetConcat(For(1,numOutput,Set(Real i)
    {
      SetConcat(For(1,numOutput,Set(Real j)
      {
        For(1,maxDegree,Real(Real d)
        {
          Real x = -Coef(PHI[i][j],d);
        //WriteLn("TRACE [@VAR.Normal::Build] 11");
          Text MPM::@ModuleVAR::getName.Phi("OUTPUT."<<i,".OUTPUT."<<j,1,d);
        //WriteLn("TRACE [@VAR.Normal::Build] 12");
          x
        })
      }))
    }))<<
    If(!hasConstant,Empty,For(1,numOutput,Real(Real k)
    {
      Real x = cte[k];
      Eval("CTE.OUTPUT."<<k+"=x")
    }))<<
    If(Card(garchDef),Empty,
    For(1,numOutput,Real(Real k)
    {
      Real x = VMatDat(covariance,k,k);
      Eval("Variance.OUTPUT."<<k+"=x")
    }))<<
    SetConcat(For(2,numOutput,Set(Real i)
    {
      For(1,i-1,Real(Real j)
      {
        Real x = VMatDat(correlation,i,j);
        Eval("Correlation.OUTPUT."<<i+".OUTPUT."<<j+"=x")
      })
    }))<<
    If(!Card(garchDef),Empty,
    {
      SetConcat(For(1,numOutput,Set(Real k)
      {
        @NameBlock g = @NameBlock((garchInfo[k])::garch);
        
        For(1,$g::_.n,Real(Real j)
        {
          Text p = $g::getParamName(j);
          Real v = $g::getParamValue(p);
          Eval(p+"=v")
        }) 
      })) 
    });
  //WriteLn("TRACE [@VAR.Normal::Build] End");
    True
  }
};
 
